﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPLabWork1
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Demonstration of work of the class \"Velocity\"");
            Console.WriteLine();

            Console.Write("Set the first vehicle's velocity in meters per second: ");
            double velocityInMps = Convert.ToDouble(Console.ReadLine());
            var firstVehicleRate = Velocity.SetFromMps(velocityInMps); 

            Console.Write("Set the second vehicle's velocity in kilometers per hour: ");
            double velocityInKph = Convert.ToDouble(Console.ReadLine());
            var secondVehicleRate = Velocity.SetFromKph(velocityInKph);
            Console.WriteLine();

            Console.WriteLine("The first vehicle's velocity in meters per second: " + firstVehicleRate.mps);
            Console.WriteLine("The first vehicle's velocity in kilometers per hour: " + firstVehicleRate.kph);
            Console.WriteLine("The first vehicle's velocity in miles per hour: " + firstVehicleRate.mph);
            Console.WriteLine();
            Console.WriteLine("The second vehicle's velocity in meters per second: " + secondVehicleRate.mps);
            Console.WriteLine("The second vehicle's velocity in kilometers per hour: " + secondVehicleRate.kph);
            Console.WriteLine("The second vehicle's velocity in miles per hour: " + secondVehicleRate.mph);
            Console.WriteLine();

            var velocitiesSum = firstVehicleRate.Add(secondVehicleRate);
            Velocity velocitiesDifference;
            if(firstVehicleRate.mps > secondVehicleRate.mps)
                velocitiesDifference = firstVehicleRate.Subtract(secondVehicleRate);
            else
                velocitiesDifference = secondVehicleRate.Subtract(firstVehicleRate);
            Console.WriteLine("The sum of first and second vehicle's velocities: " + velocitiesSum.mps);
            Console.WriteLine("The difference of first and second vehicle's velocities: " + velocitiesDifference.mps);
            Console.WriteLine();

            Console.WriteLine("Press any key to terminate the program");
            Console.ReadKey();
        }
    }
}
