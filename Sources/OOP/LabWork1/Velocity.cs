﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPLabWork1
{
    class Velocity
    {
        #region Constants
        protected const double KphToMphRatio = 1.609;
        protected const double KphToMpsRatio = 3.600;
        protected const double MpsToKphRatio = 0.277;
        protected const double MphToKphRatio = 0.621;
        #endregion Constants

        protected readonly double _metersPerSecond;

        #region Properties
        public double mps
        {
            get
            {
                return (_metersPerSecond);
            }
        }

        public double mph
        {
            get
            {
                return (_metersPerSecond * KphToMpsRatio * MphToKphRatio);
            }
        }

        public double kph
        {
            get
            {
                return (_metersPerSecond * KphToMpsRatio);
            }
        }
        #endregion Properties

        private Velocity(double valueInMps)
        {
            if (valueInMps < 0)
                throw new ArgumentException("Velocity can't be negative", "valueInMps");
            _metersPerSecond = valueInMps;
        }

        #region Creation Methods
        static public Velocity SetFromMps(double valueInMps)
        {
            var temp = new Velocity(valueInMps);

            return temp;
        }

        static public Velocity SetFromKph(double valueInKph)
        {
            Velocity temp;
            double valueInMps;

            valueInMps = valueInKph * MpsToKphRatio;

            temp = new Velocity(valueInMps);

            return temp;
        }
        #endregion Creation Methods

        #region Operations
        public Velocity Add(Velocity summand)
        {
            double valueInMps;

            valueInMps = _metersPerSecond + summand.mps;

            var sum = new Velocity(valueInMps);

            return sum;
        }

        public Velocity Subtract(Velocity subtrahend)
        {
            double valueInMps;

            valueInMps = _metersPerSecond - subtrahend.mps;

            var difference = new Velocity(valueInMps);

            return difference;
        }
        #endregion Operations
    }
}
